# simple_systemC

##First Example
simple_systemc\firstExample

 This is my first Systemc Example
 Design Name : first_counter
 File Name : first_counter.cpp
 Function : This is a 4 bit up-counter with
 Synchronous active high reset and
 with active high enable signal

#Counter Design Specs#

4-bit synchronous up counter.
active high, asynchronous reset.
Active high enable.

#Counter Test Bench#

Any digital circuit, no matter how complex, needs to be tested. For the counter logic, 
we need to provide a clock and reset logic. Once the counter is out of reset, we toggle
the enable input to the counter, and check the waveform to see if the counter is counting
correctly. The same is done in SystemC. Counter testbench consists of clock generator, reset control,
enable control and monitor/checker logic. Below is the simple code of testbench without the monitor/checker logic.


##Simple implementation for Inter IC bus system.

###Usage
Run this command to compile and simulate SystemC project 
SC_SIGNAL_WRITE_CHECK=DISABLE make run



